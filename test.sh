#!/bin/sh

set -eu
set -x

# Perhaps can use $CI_PIPELINE_SOURCE to decide if we're storing
# results in qa-reports or staging-qa-reports
echo "INFO: Pipeline triggered by: '${CI_PIPELINE_SOURCE}'"

# Used by squad-client
export SQUAD_HOST="https://staging-qa-reports.linaro.org"
export SQUAD_TOKEN="${STAGING_QA_REPORTS_TOKEN}"

# Qa-Reports group/project
export QA_REPORTS_GROUP="blueprints"
export QA_REPORTS_PROJECT="ewaol"
export QA_REPORTS_LAVA_BACKEND="ledge.validation.linaro.org"
export QA_REPORTS_LAVA_DEVICE_TYPE="qemu_arm64"

# LAVA job definition template
LAVA_TEMPLATE_NAME="ewaol-boot.yaml.jinja2"

./build-results.py output/results.json | tee log.txt

BUILD_STATUS=`tail -f log.txt`

if [ "${BUILD_STATUS}" = "pass" ]; then
    BASE_ARTIFACTS_URL="${CI_PROJECT_URL}/-jobs/${BUILD_JOB_ID}/artifacts/raw/output"

    # Get kernel image file name
    KERNEL_FILENAME=`cd output && ls ewaol-baremetal-image-generic-arm64-*.rootfs.wic.bz2`

    ${CI_PROJECT_DIR}/lava/generate_job_definition.py > definition.yml \
        --template-name "${LAVA_TEMPLATE_NAME}" \
        --overwrite-variables \
            "cert_image_url=https://people.linaro.org/~antonio.terceiro/ts/ledge-kernel-uefi-certs.ext4.img" \
            "rootfs_url=${BASE_ARTIFACTS_URL}/${KERNEL_FILENAME}" \
            "bios_url=${BASE_ARTIFACTS_URL}/flash.bin-qemu"

    squad-client \
        submit-job \
        --group ${QA_REPORTS_GROUP} \
        --project "${QA_REPORTS_PROJECT}" \
        --build ${BUILD_JOB_ID} \
        --backend "${QA_REPORTS_LAVA_BACKEND}" \
        --environment "${QA_REPORTS_LAVA_DEVICE_TYPE}" \
        --definition definition.yml
fi
