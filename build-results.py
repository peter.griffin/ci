#!/usr/bin/env python3

import json
import sys
import os

from squad_client.shortcuts import submit_results
from squad_client.core.api import SquadApi


SquadApi.configure(os.getenv('SQUAD_HOST'), os.getenv('SQUAD_TOKEN'))
group_and_project = f'{os.getenv("QA_REPORTS_GROUP")}/{os.getenv("QA_REPORTS_PROJECT")}'
build_version = os.getenv('BUILD_JOB_ID')
env = os.getenv('QA_REPORTS_LAVA_DEVICE_TYPE')
build_url = f'{os.getenv("CI_PROJECT_URL")}/-/jobs/{os.getenv("BUILD_JOB_ID")}'

def main():
    global build_url

    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} results.json')
        return

    results_file = sys.argv[1]
    results = json.load(open(results_file))

    squad_results = {}
    pass_fail = []
    for key, build in results['builds'].items():
        name = build['name']
        print(f'Submitting {name}')

        metadata = { 
            'download_url': build['download_url'],
            'build_log': f'{build["download_url"]}/build.log',
            'repositories': build['sources']['kas'],
        }

        # Send build_url only once
        if build_url != '':
            metadata['build_url'] = build_url
            build_url = '' 

        submit_results(
            group_project_slug=group_and_project,
            build_version=build_version,
            env_slug=env,
            tests={
                f'build/{name}': build['result'],
            },
            metrics={
                f'build/{name}-duration': build['duration'],
            },
            metadata=metadata
        )

        pass_fail.append(build['result'] == 'pass')

    # Print out so the outter script can know if all tests passed
    print('pass' if all(pass_fail) else 'fail')


if __name__ == '__main__':
    main()
