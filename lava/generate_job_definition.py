#!/usr/bin/env python3

# Copied from: https://gitlab.com/OVSS/ci/pipelines/-/blob/main/lava/generate_lava_template.py

import argparse
import logging
import os
import yaml
from jinja2 import (
    Environment,
    FileSystemLoader,
    StrictUndefined,
)


FORMAT = "[%(funcName)16s() ] %(message)s"
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--overwrite-variables",
        help="Key-value pairs overwriting variables from the file",
        nargs="+",
        dest="overwrite_variables",
        default=[],
    )
    parser.add_argument(
        "--template-path",
        help="Path to LAVA job templates",
        dest="template_path",
        default="lava/templates",
    )
    parser.add_argument(
        "--template-name",
        help="Name of the LAVA job template",
        dest="template_name",
        required=True,
    )
    parser.add_argument(
        "--verbose",
        help="""Verbosity level. Follows logging levels:
                          CRITICAL: 50
                          ERROR: 40
                          WARNING: 30
                          INFO: 20
                          DEBUG: 10
                          NOTSET: 0""",
        dest="verbose",
        type=int,
        default=logging.INFO,
    )

    args = parser.parse_args()
    logger.setLevel(args.verbose)
    THIS_DIR = os.path.abspath(args.template_path)

    j2_env = Environment(
        loader=FileSystemLoader(THIS_DIR, followlinks=True), undefined=StrictUndefined
    )

    context = {}
    context.update(os.environ)

    for variable in args.overwrite_variables:
        key, value = variable.split("=")
        context.update({key: value})

    lava_job = j2_env.get_template(args.template_name).render(context)
    print(yaml.dump(yaml.safe_load(lava_job), default_flow_style=False, sort_keys=False))


if __name__ == "__main__":
    main()
