#!/bin/sh

set -eu

mkdir output
tuxsuite plan --json --json-out output/results.json trs.yml
python3 download.py --output-directory=output/ trs.yml output/results.json

# CI_JOB_ID helps bulding the URL for accessing this job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
