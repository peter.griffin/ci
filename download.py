#!/usr/bin/env python3

import argparse
import itertools
import json
import pathlib
import requests
import subprocess
import yaml

MB = 1024 ** 2

def download_artifacts(build_request, results, output_directory):
    # for build in results["oebuilds"]["results"]:  # tuxsuite plan get --json KSUID
    for ksuid, build in results["builds"].items(): # tuxsuite plan --json PLAN.yaml
        if build["sources"] == build_request["sources"]:
            download_url = build["download_url"]
            for key, artifact in build_request.get("artifacts", {}).items():
                path = pathlib.Path(artifact)
                directory = str(path.parent)
                name = path.name
                # FIXME: this will download the listing of a directory for every artifact in it
                listing = requests.get(f"{download_url}{directory}", params={"export": "json"})
                files = listing.json()["files"]
                for item in files:
                    url = item["Url"]
                    filename = pathlib.Path(url.split("/")[-1])
                    if filename.match(name):
                        print(f"Downloading {url} ", end="", flush=True)
                        download = requests.get(url, stream=True)
                        with (output_directory / filename).open("wb") as f:
                            for chunk in download.iter_content(chunk_size=MB):
                                f.write(chunk)
                                print(".", end="", flush=True)
                        print(" OK")


def main():
    parser = argparse.ArgumentParser(description="download artifacts from TuxSuite")
    parser.add_argument("--output-directory", "-o", type=pathlib.Path, default=pathlib.Path("."))
    parser.add_argument("plan", type=pathlib.Path)
    parser.add_argument("results", type=pathlib.Path)
    options = parser.parse_args()
    plan = yaml.safe_load(options.plan.open())
    results = json.load(options.results.open())

    options.output_directory.mkdir(exist_ok=True)
    for job in plan["jobs"]:
        for build in job["bakes"]:
            download_artifacts(build, results, options.output_directory)

if __name__ == "__main__":
    main()
